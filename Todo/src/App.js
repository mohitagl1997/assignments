import React,{Component} from 'react';
import './App.css';
import Add from './Add';

class App extends Component {  
  state={
    items:[]
  }
  
  addItem=(k) => {      
    if(k.key==='Enter' && k.target.value!='')
    {             
      this.setState({        
        items:[...this.state.items, k.target.value]        
      })     
      document.querySelector(".text").value="";                 
    }
  }

  deleteEvent = (k) =>{    
    this.setState({})    
    this.state.items.splice(k,1)    
  }

  editEvent = (k) =>{    
    let temp=this.state.items[k];
    let getPrompt=prompt(this.state.items[k]);
    this.setState({})
    if(getPrompt!==null)
    {
        this.state.items[k]=getPrompt;
    }
    else
    {
        this.state.items[k]=temp;
    }
  }

  render(){
    return (
      <div className="App">
        <div className="container">
          <h1>ToDo List</h1>     
          <input type="text" className="text" onKeyDown={this.addItem} />
          <div className="list">
            {
              this.state.items.map((item,index) => {                
                return(
                  <Add title={item} ind={index} delete={this.deleteEvent} edit={this.editEvent} />
                )
              })              
            }            
          </div>
        </div>
      </div>
    );
  }
}

export default App;
