import React,{useState} from 'react';
import './App.css';

const Add=(props) => {    
    return ( 
        <div>
            <p id="p">{props.title}</p>
            <button id="btn" onClick={()=>props.delete(props.ind)}>&#x274c;</button>
            <button id="btn" onClick={()=>props.edit(props.ind)}>&#128393;</button>            
            <hr className="hr" />
        </div>                                
    );
}

export default Add;